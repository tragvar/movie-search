//
//  MovieService.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper


class MovieService {

    func fetchMovies(with query: String,
                     page: Int,
                     success: @escaping ((_ result: [Movie]) -> ()),
                     failure: @escaping (_ error: Error) -> ()) {
        
        AF.request(RequestBuilder.fetchMovies(query, page)).responseObject { (response: AFDataResponse<MovieResponse>) in

            if let error = response.error {
                failure(error)
                return
            }

            guard let movies = response.value?.movies else {
                return
            }

            success(movies)
        }
    }

    func fetchMovieDetails(with movieId: Int,
                           success: @escaping ((_ result: MovieDetails) -> ()),
                           failure: @escaping (_ error: Error) -> ()) {

        AF.request(RequestBuilder.fetchMovieDetails(movieId)).responseObject { (response: AFDataResponse<MovieDetails>) in

            if let error = response.error {
                failure(error)
                return
            }

            guard let details = response.value else {
                return
            }

            success(details)
        }
    }
}
