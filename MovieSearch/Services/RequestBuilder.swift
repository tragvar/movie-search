//
//  RequestBuilder.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation
import Alamofire

enum RequestBuilder: URLRequestConvertible {

    case fetchMovies(String, Int)
    case fetchMovieDetails(Int)


    func asURLRequest() throws -> URLRequest {
        var method: HTTPMethod {
            switch self {
            case .fetchMovies(_,_):
                return .get

            case .fetchMovieDetails(_):
                return .get
            }
        }

        let url:URL = {
            let path: String?

            switch self {
            case .fetchMovies(let query, let pageNumber):
                let encodedQuery = query.encodeForURL()
                path = "/search/movie?api_key=\(Constants.API.key)&query=\(encodedQuery)&page=\(pageNumber)"

            case .fetchMovieDetails(let movieID):
                path = "/movie/\(movieID)?api_key=\(Constants.API.key)"
            }

            var urlString = Constants.API.baseURL
            if let path = path {
                urlString.append(path)
            }
            return URL(string: urlString)!
        }()

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest)
    }
}

