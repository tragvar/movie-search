//
//  Movie.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieResponse: Mappable {

    var movies: [Movie]?    // results

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        movies  <-  map["results"]
    }
}

public struct Movie: Mappable {

    var identifier: Int?        // id
    var title: String?          // title
    var releaseDate: String?    // release_date
    var posterPath: String?     // poster_path

    public init?(map: Map) {}

    mutating public func mapping(map: Map) {
        identifier  <-  map["id"]
        title       <-  map["title"]
        releaseDate <-  map["release_date"]
        posterPath  <-  map["poster_path"]
    }
}

