//
//  MovieDetails.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation
import ObjectMapper

public struct MovieDetails: Mappable {

    var identifier: Int?                        // id
    var title: String?                          // title
    var genres: [[String : Any]]?               // genres
    var homepage: String?                       // homepage
    var overview: String?                       // overview
    var releaseDate: String?                    // release_date
    var backdropPath: String?                   // backdrop_path
    var posterPath: String?                     // poster_path
    var productionCompanies: [[String : Any]]?  // production_companies

    public init?(map: Map) {}

    mutating public func mapping(map: Map) {

        identifier          <-  map["id"]
        title               <-  map["title"]
        genres              <-  map["genres"]
        homepage            <-  map["homepage"]
        backdropPath        <-  map["backdrop_path"]
        posterPath          <-  map["poster_path"]
        overview            <-  map["overview"]
        releaseDate         <-  map["releaseDate"]
        productionCompanies <-  map["production_companies"]
    }
}
