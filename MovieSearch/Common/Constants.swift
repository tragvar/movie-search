//
//  Constants.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation

struct Constants {

    struct API {
        static let key = "c33e25174af866c5c102772d92d0e480"
        static let baseURL = "https://api.themoviedb.org/3"
        static let poster185BaseUrl = "https://image.tmdb.org/t/p/w185"
        static let posterOriginalBaseUrl = "https://image.tmdb.org/t/p/original"
        static let backdropOriginalBaseUrl = "https://image.tmdb.org/t/p/original"
    }
}

/*
 "backdrop_sizes": [
 "w300",
 "w780",
 "w1280",
 "original"
 ],
 "logo_sizes": [
 "w45",
 "w92",
 "w154",
 "w185",
 "w300",
 "w500",
 "original"
 ],
 "poster_sizes": [
 "w92",
 "w154",
 "w185",
 "w342",
 "w500",
 "w780",
 "original"
 ],
 "profile_sizes": [
 "w45",
 "w185",
 "h632",
 "original"
 ],
 "still_sizes": [
 "w92",
 "w185",
 "w300",
 "original"
 ]
*/
