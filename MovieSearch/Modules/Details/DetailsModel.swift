//
//  DetailsModel.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

public protocol DetailsModelDelegate: NSObjectProtocol {

    func modelDidChanged(model: DetailsModelProtocol)
}

public protocol DetailsModelProtocol: NSObjectProtocol {

    var delegate: DetailsModelDelegate? { get set }
    var movieDetails: MovieDetails? { get }
}

public class DetailsModel: NSObject, DetailsModelProtocol {

    weak public var delegate: DetailsModelDelegate?
    public private(set) var movieDetails: MovieDetails?


    required public init(with identifier: Int) {

        super.init()
        self.fetchMoviesDetails(movieId: identifier)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func getMovieDetails() -> MovieDetails {
        return self.movieDetails!
    }


    // MARK: - DetailsModel methods

    /** Implement DetailsModel methods here */

    func fetchMoviesDetails(movieId: Int) {
        MovieService().fetchMovieDetails(with: movieId, success: { (aMovieDetails) in

            self.movieDetails = aMovieDetails
            self.delegate?.modelDidChanged(model: self)

        }) { (error) in

        }
    }

    // MARK: - Private methods

    /** Implement private methods here */

}

