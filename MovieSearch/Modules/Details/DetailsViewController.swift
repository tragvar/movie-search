//
//  DetailsViewController.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit
//import KVNProgress

typealias DetailsViewControllerType = BaseViewController<DetailsModelProtocol, DetailsViewProtocol, DetailsRouter>

class DetailsViewController: DetailsViewControllerType {

    // MARK: Initializers

    required public init(withView view: DetailsViewProtocol!, model: DetailsModelProtocol!, router: DetailsRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        if model.movieDetails == nil {
//            KVNProgress.show(withStatus: "Loading...", on: self.view)
        }
        setupNavigationBar()
        customView.delegate = self
        model.delegate = self
    }

    private func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = self.model.movieDetails?.title
    }
}



// MARK: - DetailsViewDelegate

extension DetailsViewController: DetailsViewDelegate {

    public func viewSomeAction(view: DetailsViewProtocol) {
    }
}

// MARK: - DetailsModelDelegate

extension DetailsViewController: DetailsModelDelegate {

    func modelDidChanged(model: DetailsModelProtocol) {
        self.title = model.movieDetails?.title!
        self.customView.configWith(details: model.movieDetails!)
//        KVNProgress.dismiss()
    }
}

