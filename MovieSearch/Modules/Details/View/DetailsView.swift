//
//  DetailsView.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import UIKit
//import SDWebImage

public protocol DetailsViewDelegate: NSObjectProtocol {

    func viewSomeAction(view: DetailsViewProtocol)
}

public protocol DetailsViewProtocol: NSObjectProtocol {

    var delegate: DetailsViewDelegate? { get set }

    func configWith(details: MovieDetails)
}

public class DetailsView: UIView, DetailsViewProtocol{


    weak public var delegate: DetailsViewDelegate?
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var cntentView: UIView!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var productionCompaniesLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!

    // MARK: - Overrided methods

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.scrollView.alwaysBounceVertical = true
    }


    // MARK: - DetailsView interface methods

    public func configWith(details: MovieDetails) {

        var genresString = ""
        details.genres?.forEach({ (genre) in
            genresString.append("\(genre["name"] as! String)\n")
        })

        var companiesString = ""
        details.productionCompanies?.forEach({ (company) in
            companiesString.append("\(company["name"] as! String)\n")
        })

        self.overviewLabel.text = details.overview
        self.productionCompaniesLabel.text = companiesString
        self.genresLabel.text = genresString
        self.layoutIfNeeded()

        print("url: \(Constants.API.backdropOriginalBaseUrl)\(details.backdropPath ?? "")")

        self.backdropImageView.sd_setImage(with: URL(string: "\(Constants.API.backdropOriginalBaseUrl)\(details.backdropPath ?? "")")!, placeholderImage: UIImage(named: "empty_image.png"))
        self.backgroundImageView.sd_setImage(with: URL(string: "\(Constants.API.posterOriginalBaseUrl)\(details.posterPath ?? "")")!, placeholderImage: UIImage(named: "empty_image.png"))

        self.scrollView.contentSize.height = self.backdropImageView.height() + self.cntentView.height()
    }

    // MARK: - IBActions

    func someButtonAction() {

        self.delegate?.viewSomeAction(view: self)
    }
}

