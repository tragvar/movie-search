//
//  DetailsBuilder.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

class DetailsBuilder: NSObject {

    class func viewControllerWith(identifier : Int) -> DetailsViewController {

        let view: DetailsViewProtocol = DetailsView.create()
        let model: DetailsModelProtocol = DetailsModel(with: identifier)
        let router = DetailsRouter()

        let viewController = DetailsViewController(withView: view, model: model, router: router)
        return viewController
    }
}

