//
//  SearchViewController.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit
import KVNProgress

public typealias SearchViewControllerType = BaseViewController<SearchModelProtocol, SearchViewProtocol, SearchRouter>

public class SearchViewController: SearchViewControllerType, UITableViewDelegate {

    private var dataSource: SearchDataSource!

    private var pageNumber: Int = 1
    private var isLoading = false

    // MARK: - Initializers

    convenience init(withView view: SearchViewProtocol, model: SearchModelProtocol, router: SearchRouter, dataSource: SearchDataSource) {

        self.init(withView: view, model: model, router: router)

        self.model.delegate = self
        self.dataSource = dataSource
        self.dataSource.cellDelegate = self

        // your custom code
    }

    public required init(withView view: SearchViewProtocol!, model: SearchModelProtocol!, router: SearchRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        customView.delegate = self
        connectTableViewDependencies()

        isLoading = true
        KVNProgress.show(withStatus: "Loading...", on: self.view)
        self.model.fetchMoviesWith(query: "movie", pageNumber: 1)
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        self.navigationController?.navigationBar.tintColor = UIColor.movieBackColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.movieTextColor()
        self.title = "Movies List of \"\(model.searchingText!)\""
    }

    private func connectTableViewDependencies() {
        customView.tableView.delegate = self
        dataSource.regicterNibsForTableView(tableView: customView.tableView)
        customView.tableView.dataSource = dataSource
    }

    // MARK: - Table view delegate

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        self.customView.disactiveSearch()
        router?.navigateToDetailsScreen(from: self, withMovieId: self.model.movies[indexPath.row].identifier!)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset

        if deltaOffset <= -64 {
            if isLoading { return }

            isLoading = true
            pageNumber += 1
            KVNProgress.show(withStatus: "Loading...", on: self.view)
            self.model.fetchMoviesWith(query: model.searchingText!, pageNumber: pageNumber)
        }
    }
}

// MARK: - SearchViewDelegate

extension SearchViewController: SearchViewDelegate {

    public func viewSearchButtonClicked(view: SearchViewProtocol, searchBar: UISearchBar) {
        self.model.resetData()
        pageNumber = 1
        KVNProgress.show(withStatus: "Loading...", on: self.view)
        self.customView?.tableView.reloadData()
        self.model.fetchMoviesWith(query: (searchBar.text)!, pageNumber: pageNumber)
    }

    public func viewSearchTextDidBeginEditing(view: SearchViewProtocol, searchBar: UISearchBar) {
        isLoading = true
    }

    public func viewSearchTextDidChange(view: SearchViewProtocol, searchBar: UISearchBar) {

    }

    public func viewSearchCancelButtonClicked(view: SearchViewProtocol, searchBar: UISearchBar) {
        isLoading = false
    }
}

// MARK: - SearchModelDelegate

extension SearchViewController: SearchModelDelegate {

    public func modelDidChanged(model: SearchModelProtocol) {
        self.title = "Movies List of \"\(model.searchingText!)\""
        self.customView?.tableView.reloadData()
        self.customView.disactiveSearch()
        KVNProgress.dismiss()
        isLoading = false
    }
}

// MARK: - SearchCellDelegate

extension SearchViewController: SearchCellDelegate {

    func cellDidTapSomeButton(cell: SearchTableViewCell) {
    }
}

