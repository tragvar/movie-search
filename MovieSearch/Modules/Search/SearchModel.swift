//
//  SearchModel.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

public protocol SearchModelDelegate: NSObjectProtocol {

    func modelDidChanged(model: SearchModelProtocol)
}

public protocol SearchModelProtocol: NSObjectProtocol {

    var delegate: SearchModelDelegate? { get set }
    var movies: [Movie] { get }
    var searchingText: String? { get }

    func fetchMoviesWith(query: String, pageNumber: Int)
    func resetData()
}

public class SearchModel: NSObject, SearchModelProtocol {

    override init() {
        self.movies = [Movie]()
        self.searchingText = ""
        super.init()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func getMovies() -> [Movie] {
        return self.movies
    }

    // MARK: - SearchModel methods

    weak public var delegate: SearchModelDelegate?
    public private(set) var movies: [Movie]
    public private(set) var searchingText: String?

    /** Implement SearchModel methods here */

    public func fetchMoviesWith(query: String, pageNumber: Int) {
        self.searchingText = query
        MovieService().fetchMovies(with: query, page: pageNumber, success: { (aMovies) in

            self.movies.append(contentsOf: aMovies)
            self.delegate?.modelDidChanged(model: self)

        }) { (error) in

        }
    }

    public func resetData() {
        self.movies = [Movie]()
    }


    // MARK: - Private methods

    /** Implement private methods here */

}

