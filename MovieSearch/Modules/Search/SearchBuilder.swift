//
//  SearchBuilder.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

public class SearchBuilder: NSObject {

    public class func viewController() -> SearchViewController {
        let view: SearchViewProtocol = SearchView.create()
        let model: SearchModelProtocol = SearchModel()
        let dataSource = SearchDataSource(withModel: model)
        let router = SearchRouter()

        let viewController = SearchViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}

