//
//  SearchRouter.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

public class SearchRouter: NSObject {

    func navigateToDetailsScreen(from vc: SearchViewController, withMovieId movieId: Int) {

        let detailsScreenVC = DetailsBuilder.viewControllerWith(identifier: movieId)

        if UI_USER_INTERFACE_IDIOM() == .pad {
            detailsScreenVC.modalPresentationStyle = .pageSheet
            detailsScreenVC.modalTransitionStyle = .crossDissolve

            vc.navigationController?.present(detailsScreenVC, animated: true, completion: nil)
        } else {
            vc.navigationController?.pushViewController(detailsScreenVC, animated: true)
        }
    }
}

