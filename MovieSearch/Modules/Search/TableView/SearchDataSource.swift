//
//  SearchDataSourceDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit
import SDWebImage

class SearchDataSource: NSObject, UITableViewDataSource {

    weak var cellDelegate: SearchCellDelegate?
    private let model: SearchModelProtocol

    init(withModel model: SearchModelProtocol) {
        self.model = model
    }

    func regicterNibsForTableView(tableView: UITableView) {
        SearchTableViewCell.register(for:tableView)
    }

    // MARK: - Private methods

    private func configure(cell: SearchTableViewCell, forMovie movie: Movie) {
        cell.titleLabel.text = movie.title
        cell.releaseDateLabel.text = movie.releaseDate
        cell.posterImageView.sd_setImage(with: URL(string: "\(Constants.API.poster185BaseUrl)\(movie.posterPath ?? "")")!, placeholderImage: UIImage(named: "empty_image.png"))
    }

    // MARK: - UITableViewDataSource

    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell: SearchTableViewCell = tableView.deque(for: indexPath)
        cell.delegate = cellDelegate

        let movie = self.model.movies[indexPath.row];
        self.configure(cell: cell, forMovie: movie)

        return cell
    }
}

