//
//  SearchView.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//Copyright © 2018 tragvar. All rights reserved.
//

import UIKit

public protocol SearchViewDelegate: NSObjectProtocol {

    func viewSearchTextDidBeginEditing(view: SearchViewProtocol, searchBar: UISearchBar)
    func viewSearchTextDidChange(view: SearchViewProtocol, searchBar: UISearchBar)
    func viewSearchCancelButtonClicked(view: SearchViewProtocol, searchBar: UISearchBar)
    func viewSearchButtonClicked(view: SearchViewProtocol, searchBar: UISearchBar)
}

public protocol SearchViewProtocol: NSObjectProtocol {

    var delegate: SearchViewDelegate? { get set }
    var tableView: UITableView! { get }

    func disactiveSearch()
}

public class SearchView: UIView, SearchViewProtocol {

    weak public var delegate: SearchViewDelegate?
    @IBOutlet weak public var tableView: UITableView!

    var searchController: UISearchController!

    // MARK: - Overrided methods

    override public func awakeFromNib() {
        super.awakeFromNib()

        configureSearchController()
    }

    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = UIColor.movieBackColor()
        searchController.searchBar.tintColor = UIColor.movieTextColor()
        searchController.searchBar.placeholder = "Enter a search request"

        tableView.tableHeaderView = searchController.searchBar
    }

    // MARK: - SearchViewView interface methods

    public func disactiveSearch() {
        searchController.isActive = false
    }
}

extension SearchView: UISearchBarDelegate {

    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.delegate?.viewSearchTextDidBeginEditing(view: self, searchBar: searchBar)
    }

    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.delegate?.viewSearchTextDidChange(view: self, searchBar: searchBar)
    }

    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.delegate?.viewSearchCancelButtonClicked(view: self, searchBar: searchBar)
    }

    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.delegate?.viewSearchButtonClicked(view: self, searchBar: searchBar)
    }
}
