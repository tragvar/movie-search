//
//  UIView+Extension.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func height() -> CGFloat {
        return self.frame.height
    }
}
