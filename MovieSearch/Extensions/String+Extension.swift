//
//  String+Extension.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation

extension String {

    func encodeForURL() -> String {
        guard
            let encoded = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                return ""
        }
        return encoded
    }
}
