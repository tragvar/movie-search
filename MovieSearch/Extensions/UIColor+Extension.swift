//
//  UIColor+Extension.swift
//  MovieSearch
//
//  Created by Illya Kostyuk on 2/3/18.
//  Copyright © 2018 tragvar. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    class func movieTextColor() -> UIColor {
        let textColor = UIColor(displayP3Red: 11.0/255, green: 212.0/255, blue: 117.0/255, alpha: 1.0)
        return textColor
    }

    class func movieBackColor() -> UIColor {
        let textColor = UIColor(displayP3Red: 8.0/255, green: 28.0/255, blue: 36.0/255, alpha: 1.0)
        return textColor
    }

}
